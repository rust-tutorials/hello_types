fn main() {
    println!("Hello, types");

//  // Primatives
    let tru: bool = true;
    println!("  true: {}", tru);

    let hearts: char = '💕';
    println!("  char: {}", hearts);

    let num_i8: i8 = i8::min_value();
    let num_i16: i16 = i16::min_value();
    let num_i32: i32 = i32::min_value();
    let num_i64: i64 = i64::min_value();
    let num_u8: u8 = u8::max_value();
    let num_u16: u16 = u16::max_value();
    let num_u32: u32 = u32::max_value();
    let num_u64: u64 = u64::max_value();
    let num_isize: isize = isize::max_value();
    let num_usize: usize = usize::max_value();
    let num_f32: f32 = std::f32::INFINITY;
    let num_f64: f64 = std::f64::MAX;
    println!("  ints: {}, {}, {} and {}", num_i8, num_i16, num_i32, num_i64);
    println!("  unsigned: {}, {}, {} and {}", num_u8, num_u16, num_u32, num_u64);
    println!("  i/u size and floats: {}, {}, {} and {}", num_isize, num_usize, num_f32, num_f64);

//  // Arrays
    let immutable_array = [1, 2, 3];
    println!("  len: {}", immutable_array.len());
    let mut mutable_array = [3, 2, 1];
    mutable_array[1] = 33;
    println!("  index: {}", mutable_array[1]);
    let fives = [5; 7];
    println!("  7th 5: {}", fives[6]);

    let full_slice = &immutable_array[..];
    println!("  full slice len: {}", full_slice.len());
    let slice = &immutable_array[1..2];
    println!("  slice len: {}", slice.len());

//  // Tupples
    let tupple = (1, "omega");
    let (dec_left, dec_right) = tupple;
    println!("  deconstructed tupple: {} and {}", dec_left, dec_right);
    println!("  indexing tupple: {} and {}", tupple.0, tupple.1);

//  // Comments
    // line comments use //
    // Doc comments use /// and support Markdown notation
    // use //! to comment containing items like the module
    // use the rustdoc tool to generate docs from doc comments
    // and run the code examples as tests


//  // Structs
    #[derive(Debug)]
    struct Point {
        x: i64,
        y: i64,
    }
    let origin = Point { x: 0, y: 0 };
    println!("  structs: {:?}", origin);

    let mut mover = Point { x: 2, y: 4 };
    mover.x = mover.y;
    mover.y = mover.y + 3;
    println!("  structs: {:?}", mover);

    let mut temp_mutabile = Point { x: -1, y: -1};
    temp_mutabile.x -= 5;
    temp_mutabile.y -= 5;
    let temp_mutabile = temp_mutabile;
    //temp_mutabile.y -= 5; // fails - struct is now immutable
    println!("  temp mutable: {:?}", temp_mutabile);

    #[derive(Debug)]
    struct MutePoint<'a> {
        x: &'a mut i64,
        y: &'a mut i64,
    }
    let mut_struct = MutePoint { x: &mut 2, y: &mut -2 };
    *mut_struct.x = *mut_struct.x - 2;
    *mut_struct.y = *mut_struct.y - 2;
    println!("  mutable struct: {:?}", mut_struct);

    #[derive(Debug)]
    struct PartMute<'a> {
        x: i64,
        y: &'a mut i64,
    }
    let part_mute = PartMute { x: 7, y: &mut 7 };
    //*part_mute.x = 0 - *part_mute.x; // fails - immutable
    *part_mute.y = 0 - *part_mute.y;
    println!("  partialy mutable: {:?}", part_mute);

    // update syntax
    let mut mute_update = Point { x: 5, y: 5 };
    mute_update = Point { x: 0, .. mute_update };
    println!("  update: {:?}", mute_update);

    // Tuple-Structs
    #[derive(Debug)]
    struct Color(i64, i64, i64);
    #[derive(Debug)]
    struct ColorAlpha(i64, i64, i64, i64);
    let black = Color(0, 0, 0);
    let sunshine = ColorAlpha(202, 170, 170, 45);
    let Color(_, g_in_black, _) = black;
    let trans = sunshine.3;
    println!("  tuplestruc: {:?} {:?}", g_in_black, trans);

    // newtype pattern
    #[derive(Debug)]
    struct Feet(i64);
    #[derive(Debug)]
    struct Meters(i64);
    let to_wall = Feet(785);
    let mut to_stop = Meters(217);
    //to_stop = to_wall; // fails - type mismatch
    println!("  distances: {:?} {:?}", to_wall, to_stop);
    fn feet_to_meters(input: &Feet) -> Meters {
        let Feet(x_feet) = *input;
        Meters((x_feet as f64 * 0.3048) as i64)
    }
    to_stop = feet_to_meters(&to_wall);
    println!("  distances: {:?} {:?}", to_wall, to_stop);

    // Unit structs
    #[derive(Debug)]
    struct EmptySet {}
    #[derive(Debug)]
    struct SymbolLike;
    let empty = EmptySet {};
    let symbol = SymbolLike;
    println!("  unit structs: {:?} {:?}", empty, symbol);

    // Enums
    #[derive(Debug)]
    enum SimpleState {
        On,
        Off,
    }
    let state = SimpleState::Off;
    println!("  enum: {:?} {:?}", SimpleState::On, state);

    #[derive(Debug)]
    enum Message {
        Stop,
        Turn(i64),
        GoTo{ x: i64, y: i64 },
    }
    let s = Message::Stop;
    let t = Message::Turn(15);
    let g = Message::GoTo{ x: 5, y: 5};
    println!("  commands: {:?} {:?} {:?}", s, t, g);

    // pattern matching
    fn show_message(msg: Message) {
        match msg {
            Message::Stop => println!("  Stop"),
            Message::GoTo { x, y } => println!("  GoTo [{},{}]", x, y),
            _ => println!("  other"),
        }
    }
    show_message(s);
    show_message(t);
    show_message(g);

    fn match_num(x: i64) {
        match x {
            4 | 6 => println!("  matching optons: 4 or 6 ()"),
            e @ 7 ... 9 => println!("  matching optons: 7-9 ({})", e),
            i if i < 0 => println!("  matching optons: negative"),
            _ => println!("  matching optons: _"),
        }
    }
    match_num(4);
    match_num(8);
    match_num(-9);
    match_num(99);


//  // Strings
    let str_string = "str strings can contain \0 null chars and
    newlines of \n    many kinds";
    println!("  str strings: {}", str_string);

    let mut string_string = "String strings".to_string();
    println!("  String strings: {}", string_string);
    string_string.push_str(" are fancy and dynamic");
    println!("  String strings: {}", string_string);

    let bytes = str_string.as_bytes();
    let char1 = str_string.chars().nth(0);
    println!("  Strings as bytes and chars: {:?} {:?}", &bytes[0..9], char1);

    let simple_concat = "String".to_string() + " + a str string";
    let coerce_concat = "String".to_string() + &" + this String needs &".to_string();
    println!("  Concatenation and coersion: {:?} {:?}", simple_concat, coerce_concat);



//  // Traits
    trait ConvertsToStandard {
        fn to_standard_unit(&self) -> f64;
        fn default_function(&self) { println!("  Default"); }
    }

    impl ConvertsToStandard for Meters {
        fn to_standard_unit(&self) -> f64 {
            self.0 as f64
        }
    }
    impl ConvertsToStandard for Feet {
        fn to_standard_unit(&self) -> f64 {
            feet_to_meters(self).0 as f64
        }
    }

    use std::fmt::Debug;
    fn print_with_standard<T: Debug + ConvertsToStandard>(meas: &T) {
        println!("  Trait and types: {:?} ==> {}", meas, meas.to_standard_unit());
    }
    to_stop.default_function();
    print_with_standard(&to_wall);
    print_with_standard(&to_stop);

//  // Trait Inheritence
    trait FancyStandard : ConvertsToStandard {
        fn fancy_print(&self);
    }
    impl FancyStandard for Feet {
        fn fancy_print(&self) {
           println!("  .. :: {} Standard Meters :: ..", self.to_standard_unit());
       }
    }
    to_wall.fancy_print();

//  // Derrivations
        // Clone
        // Copy
        // Debug
        // Default
        // Eq
        // Hash
        // Ord
        // PartialEq
        // PartialOrd


} // main
